!/bin/sh
###############################################################################
# This script will setup Debian 9 to compile Amlogic Android/Buildroot.
#
# Written by:	croniccorey (cronmod.dev@gmail.com)
# Date:		2017/09/30
###############################################################################

#######################
##     Functions     ##
#######################

check_root()
{
	echo "##########################################"
	echo "##     Checking for root permission     ##"
	echo "##########################################"

	if [ "$(id -u)" != "0" ]; then
		echo "ERROR: This script must be run as root...."
		exit 1
	fi
}

check_OS()
{
	echo "#######################################"
	echo "##     Checking operating system     ##"
	echo "#######################################"

	OS_ARCH=`uname -m`
	OS_NAME=`cat /etc/os-release | grep -w ID | awk -F "=" '{print $2}'`
	OS_NUMBER=`cat /etc/os-release | grep -w VERSION_ID | grep -o 9`
	if [ "$OS_ARCH" != "x86_64" ] || [ "$OS_NAME" != "debian" ] || [ "$OS_NUMBER" != "9" ]; then
		echo "ERROR: This script only supports Debian 9 (Stretch) 64-bit...."
		exit 1
	fi
}
	
install_packages()
{
	echo "################################"
	echo "##     Installing packages    ##"
	echo "################################"

	dpkg --add-architecture i386
	apt-get update
	apt-get install -y nfs-kernel-server autofs autoconf automake make \
	vim perl gcc g++ bc build-essential git-core gnupg flex \
	bison gperf zip unzip curl libc6-dev x11proto-core-dev subversion \
	libx11-dev:i386 libreadline-dev:i386 libgl1-mesa-dev:i386 g++-multilib \
	liblzo2-2 lzma mingw-w64 tofrodos libxml2-utils xsltproc zlib1g-dev zlib1g-dev:i386 \
	python python-markdown software-properties-common gawk libtool gettext \
	libncurses5-dev libncurses5-dev:i386 autopoint nasm flex libsdl-image1.2 \
	libxml-parser-perl texinfo wget pkg-config swig cpio liblzo2-2 whois \
	mercurial unixodbc lzop ckermit putty unrar android-tools-* u-boot-tools

	# Set default release for APT
	APT_CONF="/etc/apt/apt.conf"
	if [ ! -f $APT_CONF ]; then
		echo "APT::Default-Release \"stretch\";" > $APT_CONF
		apt-get update
	fi

	# Use jessie because these packages are missing in stretch
	SRC_LIST="/etc/apt/sources.list.d/jessie.list"
	if [ ! -f $SRC_LIST ]; then
		echo "deb http://mirror.csclub.uwaterloo.ca/debian/ jessie main" > $SRC_LIST
		apt-get update
	fi
	apt-get install -y -t jessie gcc-4.9 g++-4.9

	## Needed only for old Android SDK, causes issues with apt
	# Use sid because these packages are missing in stretch
	#SRC_LIST="/etc/apt/sources.list.d/sid.list"
	#if [ ! -f $SRC_LIST ]; then
	#	echo "deb http://mirror.csclub.uwaterloo.ca/debian/ sid main" > $SRC_LIST
	#	apt-get update
	#fi
	#apt-get install -y -t sid openjdk-7-jdk

	rm -rf /usr/lib/i386-linux-gnu/libGL.so /usr/lib/x86_64-linux-gnu/libmpc.so.2 /usr/bin/gcc /usr/bin/g++ 
	ln -s /usr/lib/i386-linux-gnu/mesa/libGL.so.1 /usr/lib/i386-linux-gnu/libGL.so
	ln -s /usr/lib/x86_64-linux-gnu/libmpc.so.3 /usr/lib/x86_64-linux-gnu/libmpc.so.2
	ln -s /usr/bin/gcc-4.9 /usr/bin/gcc
	ln -s /usr/bin/g++-4.9 /usr/bin/g++
}

install_amlogic_packages()
{
	echo "#########################################"
	echo "##     Installing Amlogic packages     ##"
	echo "#########################################"
	DROPBOX="https://www.dropbox.com"

	NAME="arc-4.8-amlogic-20130904-r2"
	if [ ! -d "/opt/$NAME" ]; then
		wget $DROPBOX/s/5pn69t7gcthuqf0/$NAME.tar.gz -P /tmp
		tar -zxvf /tmp/$NAME.tar.gz -C /opt
	fi

	NAME="CodeSourcery"
	if [ ! -d "/opt/$NAME" ]; then
		wget $DROPBOX/s/tkvnj4n6klrb3pw/$NAME.tar.gz -P /tmp
		tar -zxvf /tmp/$NAME.tar.gz -C /opt
	fi

	NAME="gcc-linaro-4.9-2016.02-x86_64_aarch64-linux-gnu"
	if [ ! -d "/opt/$NAME" ]; then
		wget $DROPBOX/s/bcjvnkshngec6xs/$NAME.tar.xz -P /tmp
		tar -Jxvf /tmp/$NAME.tar.xz -C /opt
	fi

	NAME="gcc-linaro-aarch64-none-elf-4.8-2013.11_linux"
	if [ ! -d "/opt/$NAME" ]; then
		wget $DROPBOX/s/rvved2rwjptibfb/$NAME.tar.gz -P /tmp
		tar -zxvf /tmp/$NAME.tar.gz -C /opt
	fi

	NAME="gcc-linaro-4.9-2016.02-x86_64_arm-linux-gnueabihf"
	if [ ! -d "/opt/$NAME" ]; then
		wget $DROPBOX/s/9az34ipfkrgp1n0/$NAME.tar.xz -P /tmp
		tar -Jxvf /tmp/$NAME.tar.xz -C /opt
	fi

	NAME="gnutools"
	if [ ! -d "/opt/$NAME" ]; then
		wget $DROPBOX/s/hdgh091t29o4m6u/$NAME.tar.gz -P /tmp
		tar -zxvf /tmp/$NAME.tar.gz -C /opt
	fi

	NAME="AMLTOOLSENV.sh"
	if [ ! -f "/etc/profile.d/$NAME" ]; then
		wget $DROPBOX/s/t3u95z4lqd1xo5y/$NAME -P /etc/profile.d
		chmod 644 /etc/profile.d/$NAME
		printf "\n# Source /etc/profile.d/$NAME\nif [ -r /etc/profile.d/$NAME ]; then\n	. /etc/profile.d/$NAME\nfi" >> /etc/bash.bashrc
	fi
}

install_repo()
{
	echo "#####################################"
	echo "##     Installing Google repo      ##"
	echo "#####################################"

	GOOGLE="https://storage.googleapis.com/git-repo-downloads"
	if [ ! -f "/usr/bin/repo" ]; then
		wget $GOOGLE/repo -P /usr/bin
		chmod a+x /usr/bin/repo
	fi
}

reboot_machine()
{
	echo "##############################"
	echo "##     Rebooting system     ##"
	echo "##############################"

	shutdown -r
}

#####################
##     Execute     ##
#####################

echo "This script will configure a Android/Buildroot build server, This script only supports Debian 9 (Stretch)..."
check_root
check_OS
install_packages
install_amlogic_packages
install_repo
reboot_machine
